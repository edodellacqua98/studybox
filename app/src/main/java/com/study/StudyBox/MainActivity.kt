package com.study.StudyBox

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.HapticFeedbackConstants
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_user.*
import kotlinx.android.synthetic.main.nav_header.*
import java.util.jar.Manifest

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    companion object{
        const val REQUEST_ENABLE_BT = 1
        const val REQUEST_PERMISSION = 2
    }

    private lateinit var mAuth: FirebaseAuth
    private var currentUser: FirebaseUser? = null
    private var bluetoothAdapter: BluetoothAdapter? = null

    //detecting bluetooth state changes --> If bluetooth gets turned on or off
    private val mReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        //detecting a state change
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (action == BluetoothAdapter.ACTION_STATE_CHANGED) {
                val state = intent.getIntExtra(
                    BluetoothAdapter.EXTRA_STATE,
                    BluetoothAdapter.ERROR
                )
                when (state) {
                    //Bluetooth has been turned off
                    BluetoothAdapter.STATE_OFF -> {val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE) //ask to enable
                    startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)}
                }
            }
        }
    }

    //request location permission
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == REQUEST_PERMISSION){
            if(grantResults[0] ==PackageManager.PERMISSION_DENIED){
                Toast.makeText(applicationContext, "This application cannot work without the required permission", Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Initialize Firebase
        mAuth = FirebaseAuth.getInstance()
        //initialize bluetooth
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

        //check bluetooth support on current device
        if(bluetoothAdapter==null){
            //Bluetooth is not supported
            Toast.makeText(this, "Your device does not support bluetooth, thus "+R.string.app_name.toString()+" won't work properly!", Toast.LENGTH_LONG).show()
            finish()
        }

        //check if bluetooth is enabled
        if(bluetoothAdapter?.isEnabled==false){
            //ask to enable bluetooth
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
        }

        registerReceiver(mReceiver, IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED))

        //Drawer menu setup
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(this, drawer_layout, toolbar, R.string.navigatino_drawer_open, R.string.navigatino_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        drawer_layout.addDrawerListener(object: DrawerLayout.DrawerListener{
            override fun onDrawerStateChanged(newState: Int) {}

            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {}

            override fun onDrawerClosed(drawerView: View) {}

            override fun onDrawerOpened(drawerView: View) {
                val currentUser = FirebaseAuth.getInstance().currentUser

                val database = FirebaseDatabase.getInstance().getReference("Users").child(currentUser!!.uid).child("name")

                database.addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {}

                    override fun onDataChange(p0: DataSnapshot) {
                        username_nav_header.text = p0.getValue(String::class.java)
                    }
                })
            }
        })

        nav_view.setNavigationItemSelectedListener(this)

        //setting default fragment
        if(savedInstanceState==null){
            val frag = SearchFragment()
            supportFragmentManager.beginTransaction().replace(R.id.fragment_container, frag)
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .show(frag)
                .commit()
            nav_view.setCheckedItem(R.id.action_search)
        }
    }

    //Automatically closing drawer
    override fun onBackPressed() {
        if(drawer_layout.isDrawerOpen(GravityCompat.START)){
            drawer_layout.closeDrawer(GravityCompat.START)
        }
        else
            super.onBackPressed()
    }

    override fun onStart() {
        super.onStart()
        currentUser = mAuth.currentUser

        //ask to login
        if(currentUser==null) {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }

        else{
            //ask for location permission
            if(ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED){
                requestPermissions(arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_PERMISSION)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(mReceiver)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode==REQUEST_ENABLE_BT){
            if(resultCode== Activity.RESULT_OK) {
                Toast.makeText(this, "Bluetooth enabled!", Toast.LENGTH_SHORT).show()
            }
            else {
                //if the user doesn't activate bluetooth, the app is shut
                Toast.makeText(this, "Without bluetooth "+R.string.app_name+" won't work properly!", Toast.LENGTH_LONG).show()
                finish()
            }
        }
    }

    //managing drawer options
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            //replace current fragment
            R.id.action_user ->  supportFragmentManager.beginTransaction().replace(R.id.fragment_container, UserFragment()).commit()
            R.id.action_search ->  supportFragmentManager.beginTransaction().replace(R.id.fragment_container, SearchFragment()).commit()
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    fun updateUsername(v:View?){
        val database = FirebaseDatabase.getInstance().getReference("Users").child(FirebaseAuth.getInstance().currentUser!!.uid).child("name")
        database.setValue(username_text.text.toString())
        v?.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)
        v?.clearFocus()
        user_layout.requestFocus()
        val imm:InputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(v?.windowToken, 0)
        Toast.makeText(this, "Updated Username!", Toast.LENGTH_SHORT).show()
        username_nav_header.text = (username_text.text.toString())
    }

    //sign out
    fun signOut(v: View) {
        mAuth.signOut()
        currentUser = null
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }

}
