package com.study.StudyBox

import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.shape.CornerFamily
import com.google.android.material.shape.MaterialShapeDrawable
import com.google.android.material.shape.ShapeAppearanceModel


class MessageAdapter ( private val mContext: Context, private var messages: MutableList<MyMessage>):RecyclerView.Adapter<MessageAdapter.MessageViewHolder>(){

    class MessageViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView) {
        var bubble:LinearLayout = itemView.findViewById(R.id.chat_message_bubble)
        var text:TextView = itemView.findViewById(R.id.text_content)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
        val itemView: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.message, parent, false)
        return MessageViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return messages.size
    }

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
       val message = messages[position]
        holder.text.text = message.content

        val radius = 100F

        val shapeAppearanceModel = ShapeAppearanceModel()
            .toBuilder()
            .setAllCorners(CornerFamily.ROUNDED, radius)
            .build()

        val shapeDrawable = MaterialShapeDrawable(shapeAppearanceModel)

        if(message.host){
            holder.bubble.setHorizontalGravity(Gravity.END)
            shapeDrawable.setFillColor(ContextCompat.getColorStateList(mContext,android.R.color.holo_red_light))
            shapeDrawable.setStroke(2.0f, ContextCompat.getColor(mContext,android.R.color.black))
            ViewCompat.setBackground(holder.text, shapeDrawable)
        }
        else{
            holder.bubble.setHorizontalGravity(Gravity.START)
            shapeDrawable.setFillColor(ContextCompat.getColorStateList(mContext,android.R.color.holo_green_light))
            shapeDrawable.setStroke(2.0f, ContextCompat.getColor(mContext,android.R.color.black))
            ViewCompat.setBackground(holder.text, shapeDrawable)
        }
    }


}