package com.study.StudyBox

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_create_lobby.*
import java.text.SimpleDateFormat
import java.util.*

class CreateLobbyActivity : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_lobby)

        //spinner menu
        val adapter = ArrayAdapter.createFromResource(this, R.array.categories, R.layout.spinner_item)
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item)
        spinner.adapter = adapter
    }

    fun createLobby(v: View){
        val lobby = Lobby()
        lobby.title = lobby_name.text.toString()
        lobby.description = lobby_dtext.text.toString()
        lobby.category = spinner.selectedItem.toString()
        lobby.color = getRandomMaterialColor().toString()
        lobby.id = UUID.randomUUID().toString()
        val date = Date(System.currentTimeMillis())
        val dataFormat = SimpleDateFormat("hh:mm aa", Locale.ITALIAN)
        lobby.timestamp = dataFormat.format(date)

        val intent = Intent()
        intent.putExtra("Lobby", lobby)
        setResult(Activity.RESULT_OK, intent)
        finish() //returning result to searchFragment
    }

    private fun getRandomMaterialColor(): Int {
        val rnd = Random()
        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))
    }


}