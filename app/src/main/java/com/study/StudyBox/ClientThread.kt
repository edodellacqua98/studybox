package com.study.StudyBox

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothSocket
import android.os.Message
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.util.*

class ClientThread(private val dev: String, val handler: Chat.MyHandler) : Thread() {

    var socket: BluetoothSocket? = null
    var chatThread: ChatThread? = null

    init {
        var tmp: BluetoothSocket? = null
        try {
            val device = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(dev) //remote host
            tmp =
                device.createInsecureRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805f9b34fb"))
        } catch (e: IOException) {
        }
        socket = tmp
    }

    fun write(bytes: ByteArray) {
        chatThread?.write(bytes)
    }

    override fun run() {
        BluetoothAdapter.getDefaultAdapter().cancelDiscovery()
        try {
            socket?.connect()
        } catch (e: IOException) {
            e.printStackTrace()
            try {
                socket = dev::class.java.getMethod(
                    "createRfcommSocket", Int::class.javaPrimitiveType
                ).invoke(dev, 1) as BluetoothSocket
                socket!!.connect()
            } catch (e: IOException) {
                socket!!.close()
            }
        }
        chatThread = ChatThread()
        chatThread!!.start()
    }

    inner class ChatThread : Thread() {

        var mInputStream: InputStream? = null
        var mOutputStream: OutputStream? = null

        init {
            var tmpIn: InputStream? = null
            var tmpOut: OutputStream? = null
            try {
                tmpIn = socket?.inputStream
                tmpOut = socket?.outputStream
            } catch (e: IOException) {
                socket?.close()
            }
            mInputStream = tmpIn
            mOutputStream = tmpOut
        }

        override fun run() {
            super.run()
            val buffer = ByteArray(1024)
            var numBytes: Int

            while (true) {
                try {
                    numBytes = mInputStream!!.read(buffer)
                    val readMsg = handler.obtainMessage(Chat.MESSAGE_READ, numBytes, -1, buffer)
                    readMsg.sendToTarget()
                } catch (e: IOException) {
                    break
                }
            }
        }

        fun write(bytes: ByteArray) {
            mOutputStream?.write(bytes)
            val writtenMsg: Message = handler.obtainMessage(
                Chat.MESSAGE_WRITE, -1, -1, bytes
            )
            writtenMsg.sendToTarget()
        }

        fun cancel() {
            socket?.close()
        }
    }
}