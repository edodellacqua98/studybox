package com.study.StudyBox

import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.*
import android.view.HapticFeedbackConstants
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_chat.*
import kotlinx.android.synthetic.main.fragment_user.*
import kotlinx.android.synthetic.main.nav_header.*
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class Chat : AppCompatActivity() {
    companion object{
        const val MESSAGE_WRITE = 1
        const val MESSAGE_READ = 2
        const val MESSAGE_NOT_DELIVERED = 3
    }

    private var messages = ArrayList<MyMessage>() //messages cache
    private var messageHandler = MyHandler()
    private var owner = false
    private var socket:ClientThread? = null
    private var device:String? = null //remote device to connect to
    private var boundService: HostService? = null //hosting sefvice reference
    private lateinit var username:String

    private lateinit var messageAdapter: MessageAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        owner = intent.getBooleanExtra("owner", false) //true if user is the owner of the lobby

        messageAdapter = MessageAdapter(this, messages)
        val mLayoutManager = LinearLayoutManager(applicationContext)

        messages_recycler.adapter = messageAdapter
        messages_recycler.layoutManager = mLayoutManager

        chat_title.text = intent.getStringExtra("title")

        if(owner) {
            //binding to hosting service
            val bindingIntent = Intent(this, HostService::class.java)
            this.bindService(bindingIntent, mServiceConnection, Context.BIND_AUTO_CREATE)
        } else{
            val currentUser = FirebaseAuth.getInstance().currentUser

            val database = FirebaseDatabase.getInstance().getReference("Users").child(currentUser!!.uid).child("name")

            database.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {}

                override fun onDataChange(p0: DataSnapshot) {
                    username = (p0.getValue(String::class.java).toString())
                }
            })

            //starting new chat client instance
            device = intent.getStringExtra("device") //remote device reference
            socket = ClientThread(device!!, messageHandler)
            socket!!.start()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if(owner) {
            boundService!!.sThread.handler = null
            boundService?.sThread!!.messages = messages //saving current messages
            unbindService(mServiceConnection)
        }
        else
            socket?.chatThread?.cancel() //closing client socket
    }

    fun sendMessage(v: View){
        if(owner)
            boundService?.sThread?.write(("Server: ${message_Space.text}").toByteArray())
        else {
            try {
                socket!!.write(("$username: ${message_Space.text}").toByteArray())
            }catch (e: IOException){
                finish()
            }
        }
        v.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)
        message_Space.setText("")
        val imm: InputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(v.windowToken, 0)
    }

    //managing messages on socket IO stream
    inner class MyHandler() : Handler() {

        fun updateClientCount(num:Int){
            this@Chat.runOnUiThread(object : Runnable {
                override fun run() {
                    client_count.text = num.toString()
                }
            })
        }

        override fun handleMessage(msg: Message) {
            super.handleMessage(msg)
            if(msg.what.toString() == ""){
                return
            }
            when(msg.what){
                MESSAGE_READ -> {
                    val text = String(msg.obj as ByteArray).trim()
                    var host = false
                    if(text.startsWith("Server:"))
                        host = true
                    messages.add(MyMessage(text, host))
                    messageAdapter.notifyDataSetChanged()
                }
                MESSAGE_WRITE-> {
                    val text = String(msg.obj as ByteArray).trim()
                    var host = false
                    if(text.startsWith("Server:"))
                        host = true
                    messages.add(MyMessage(text, host))
                    messageAdapter.notifyDataSetChanged()
                }
                MESSAGE_NOT_DELIVERED ->{
                    Toast.makeText(baseContext, "No client connected!", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    //Binding service
    private val mServiceConnection = object: ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName?) {
            TODO("Not yet implemented")
        }

        override fun onNullBinding(name: ComponentName?) {
            Toast.makeText(baseContext, "Not hosting this Lobby!", Toast.LENGTH_SHORT).show()
            finish()
        }

        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            val myBinder = service as HostService.MyBinder
            boundService = myBinder.getService()

            val cached_messages = boundService!!.getLobbyMessages()
            //recover messages
            for(i in 0 until cached_messages.size){
                messages.add(cached_messages[i])
                messageAdapter.notifyDataSetChanged()
            }

            boundService!!.sThread.handler = messageHandler //starting to handle messages on UI
        }

    }

}
