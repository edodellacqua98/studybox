package com.study.StudyBox

import java.io.Serializable

class Lobby() : Serializable {
    var hosted = false
    lateinit var id:String
    lateinit var title: String
    lateinit var category: String
    lateinit var description: String
    lateinit var timestamp: String
    lateinit var color: String
}