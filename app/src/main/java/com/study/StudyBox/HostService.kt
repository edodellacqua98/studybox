package com.study.StudyBox

import android.app.*
import android.bluetooth.BluetoothAdapter
import android.bluetooth.le.AdvertiseCallback
import android.bluetooth.le.AdvertiseData
import android.bluetooth.le.AdvertiseSettings
import android.content.Intent
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.os.ParcelUuid
import androidx.annotation.RequiresApi
import com.google.firebase.database.FirebaseDatabase
import java.util.*
import kotlin.collections.ArrayList


class HostService: Service() {

    private var mBinder = MyBinder()
    lateinit var sThread:ServerThread
    private var uuid:String? = null
    lateinit var lobby: Lobby

    companion object{
        const val ONGOING_NOTIFICATION_ID = 1
        const val CHANNEL_ID = "ForegroundServiceChannel"
    }

    inner class MyBinder : Binder() {
        fun getService():HostService{
            return this@HostService
            }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        //autostop after 5 minutes
        val t = Timer()
        t.scheduleAtFixedRate(object: TimerTask() {
            override fun run() {
                this@HostService.stopSelf()
            }
        }, 300000, 300000)

        lobby = intent!!.getSerializableExtra("Lobby") as Lobby
        this.uuid = intent.getStringExtra("uuid")

        createNotificationChannel()

        val notificationIntent = Intent(this, HostService::class.java)
        val pendingIntent =
            PendingIntent.getActivity(this, 0, notificationIntent, 0)

        val notification: Notification = Notification.Builder(this, CHANNEL_ID)
            .setContentTitle("Interestdeck - HOSTING")
            .setContentText("Lobby: ${lobby.title}")
            .setSmallIcon(R.mipmap.ic_launcher_round)
            .setContentIntent(pendingIntent)
            .build()

        startForeground(ONGOING_NOTIFICATION_ID, notification) //foreground service to avoid getting killed by system

        //start hosting the lobby
        sThread = ServerThread(BluetoothAdapter.getDefaultAdapter(), ParcelUuid.fromString(uuid).uuid ,null)
        sThread.start()

        //start sending advertisements
        val advertiser = BluetoothAdapter.getDefaultAdapter().bluetoothLeAdvertiser
        val settings = AdvertiseSettings.Builder()
            .setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_LOW_LATENCY)
            .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_HIGH)
            .setConnectable(false)
            .build()

        val parcelUuid = ParcelUuid(UUID.fromString(resources.getString(R.string.UUID)))

        val advertiseData = AdvertiseData.Builder()
            .setIncludeDeviceName(false)
            .addServiceUuid(parcelUuid)
            .addServiceData(ParcelUuid(UUID.fromString(lobby.id)), BluetoothAdapter.getDefaultAdapter().name!!.toByteArray())
            .build()

        advertiser.startAdvertising(settings, advertiseData, object : AdvertiseCallback() {
            override fun onStartFailure(errorCode: Int) {
                super.onStartFailure(errorCode)
                stopSelf()
            }
        })

        return START_NOT_STICKY
    }

    fun getLobbyMessages():ArrayList<MyMessage>{
        return sThread.messages
    }

    override fun onDestroy() {
        super.onDestroy()
        val database = FirebaseDatabase.getInstance().getReference("Lobbies").child(lobby.id) //remove from database
        database.removeValue()
    }

    override fun onBind(intent: Intent?): IBinder? {
        return mBinder
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(
                CHANNEL_ID,
                "Foreground Service Channel",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            val manager = getSystemService(
                NotificationManager::class.java
            )
            manager.createNotificationChannel(serviceChannel)
        }
    }
}