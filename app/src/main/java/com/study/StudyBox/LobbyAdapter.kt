package com.study.StudyBox

import android.content.Context
import android.util.SparseBooleanArray
import android.view.HapticFeedbackConstants
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlin.collections.ArrayList

class LobbyAdapter(private val mContext: Context, private var lobbies: MutableList<Lobby>, private val listener: LobbyAdapterListener) : RecyclerView.Adapter<LobbyAdapter.MyViewHolder>() {

    private val selectedItems: SparseBooleanArray = SparseBooleanArray()

    // array used to perform multiple animation at once
    private val animationItemsIndex: SparseBooleanArray = SparseBooleanArray()
    private var reverseAllAnimations = false

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnLongClickListener, View.OnClickListener {

        var title:TextView = view.findViewById(R.id.title)
        var category: TextView = view.findViewById(R.id.txt_primary)
        var description: TextView = view.findViewById(R.id.txt_secondary)
        var iconText:TextView = view.findViewById(R.id.icon_text)
        var timestamp:TextView = view.findViewById(R.id.timestamp)
        var imgProfile:ImageView = view.findViewById(R.id.icon_profile)
        var iconBack:RelativeLayout= view.findViewById(R.id.icon_back)
        var iconFront:RelativeLayout = view.findViewById(R.id.icon_front)

        var messageContainer:RelativeLayout = view.findViewById(R.id.card_layout)

        override fun onLongClick(view: View): Boolean {
            listener.onRowLongClicked(adapterPosition)
            view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)
            return true
        }

        override fun onClick(view:View){
            listener.onRowItemClicked(adapterPosition)
        }

        init {
            view.setOnLongClickListener(this)
            view.setOnClickListener(this)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.cardview, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val lobby= lobbies[position]

        holder.title.text = lobby.title
        holder.category.text = lobby.category
        holder.description.text = lobby.description
        holder.timestamp.text = lobby.timestamp

        // displaying the first letter of title in icon text
        holder.iconText.text = lobby.title.substring(0, 1)

        // change the row state to activated
        holder.itemView.isActivated = selectedItems[position, false]

        // handle icon animation
        applyIconAnimation(holder, position)

        // display profile image
        applyProfilePicture(holder, lobby)

        if(lobby.hosted)
            holder.messageContainer.setBackgroundColor(mContext.getColor(R.color.colorPrimary))

    }

    private fun applyProfilePicture(holder: MyViewHolder, lobby: Lobby) {
            holder.imgProfile.setImageResource(R.drawable.bg_circle)
            holder.imgProfile.setColorFilter(lobby.color.toInt())
            holder.iconText.visibility = View.VISIBLE
    }

    private fun applyIconAnimation(holder: MyViewHolder, position: Int) {
        if (selectedItems[position, false]) {
            holder.iconFront.visibility = View.GONE
            resetIconYAxis(holder.iconBack)
            holder.iconBack.visibility = View.VISIBLE
            holder.iconBack.alpha = 1f
            if (currentSelectedIndex == position) {
                FlipAnimator.flipView(mContext, holder.iconBack, holder.iconFront, true)
                resetCurrentIndex()
            }
        } else {
            holder.iconBack.visibility = View.GONE
            resetIconYAxis(holder.iconFront)
            holder.iconFront.visibility = View.VISIBLE
            holder.iconFront.alpha = 1f
            if (reverseAllAnimations && animationItemsIndex[position, false] || currentSelectedIndex == position
            ) {
                FlipAnimator.flipView(mContext, holder.iconBack, holder.iconFront, false)
                resetCurrentIndex()
            }
        }
    }

    // As the views will be reused, sometimes the icon appears as
    // flipped because older view is reused. Reset the Y-axis to 0
    private fun resetIconYAxis(view: View) {
        if (view.rotationY != 0F) {
            view.rotationY = 0F
        }
    }

    fun resetAnimationIndex() {
        reverseAllAnimations = false
        animationItemsIndex.clear()
    }

    override fun getItemCount(): Int {
        return lobbies.size
    }

    fun toggleSelection(pos: Int) {
        currentSelectedIndex = pos
        if (selectedItems[pos, false]) {
            selectedItems.delete(pos)
            animationItemsIndex.delete(pos)
        } else {
            selectedItems.put(pos, true)
            animationItemsIndex.put(pos, true)
        }
        notifyItemChanged(pos)
    }

    fun clearSelections() {
        reverseAllAnimations = true
        selectedItems.clear()
        notifyDataSetChanged()
    }

    fun getSelectedItems(): List<Int> {
        val items: MutableList<Int> = ArrayList(selectedItems.size())
        for (i in 0 until selectedItems.size()) {
            items.add(selectedItems.keyAt(i))
        }
        return items
    }

    private fun resetCurrentIndex() {
        currentSelectedIndex = -1
    }

    companion object {
        // index is used to animate only the selected row
        // dirty fix, find a better solution
        private var currentSelectedIndex = -1

        interface LobbyAdapterListener {
            fun onRowLongClicked(position: Int)
            fun onRowItemClicked(position:Int)
        }

    }

}