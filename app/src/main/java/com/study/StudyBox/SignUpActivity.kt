package com.study.StudyBox

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_signup.*


class SignUpActivity : AppCompatActivity() {

    private val mAuth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        //Initialize Activity Layout
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

        //Listener for checking email duplication after text changes in the edit_text
        email_view.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                try {
                    //check the email pattern
                    if (android.util.Patterns.EMAIL_ADDRESS.matcher(s).matches()) {
                        //checks firebase for the given address
                        mAuth.fetchSignInMethodsForEmail(s.toString())
                            .addOnCompleteListener { task ->
                                //no account with the same email
                                if (!task.result?.signInMethods.isNullOrEmpty()) {
                                    //TODO
                                } else
                                    sign_up_button.isEnabled = true
                                //TODO
                            }
                    }
                } catch (e: FirebaseAuthInvalidCredentialsException) {
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })
    }

    fun signUp(view: View?) {

        //Create Firebase authentication with email + password
        mAuth.createUserWithEmailAndPassword(
            email_view.text.toString(),
            password_view.text.toString()
        )
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    //Sign up succeeded
                    val user = mAuth.currentUser
                    //Json value reference
                    val id = user!!.uid
                    val userMap = HashMap<String, String>()
                    userMap["name"] = username_view.text.toString()
                    //Update name field
                    val database = FirebaseDatabase.getInstance().getReference("Users").child(id)
                    database.setValue(userMap).addOnCompleteListener { task1 ->
                        if (task1.isSuccessful) {
                            //Send verification mail
                            user.sendEmailVerification()
                            val intent = Intent(applicationContext, MainActivity::class.java)
                            startActivity(intent)
                            finish()
                        }
                    }
                } else {
                    //Sign up unsuccessful
                    Toast.makeText(this, "Failed to sign up!", Toast.LENGTH_SHORT).show()
                }
            }
    }
}
