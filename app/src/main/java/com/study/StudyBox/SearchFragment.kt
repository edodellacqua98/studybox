package com.study.StudyBox

import DividerItemDecoration
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.le.*
import android.content.*
import android.os.Build
import android.os.Bundle
import android.os.ParcelUuid
import android.view.*
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.android.synthetic.main.fragment_user.*
import kotlinx.android.synthetic.main.nav_header.*
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class SearchFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener, LobbyAdapter.Companion.LobbyAdapterListener {

    companion object{
        const val REQUEST_LOBBY_CREATION = 2
        const val REQUEST_ENABLE_DISCOVERABLE = 3
    }

    private var discoveryEnabled = false //bluetooth discoverability

    private val lobbies = ArrayList<Lobby>()
    private var devices = HashMap<String, BluetoothDevice>() // Matches device name with mac address --> For scanned devices
    private var macTable = HashMap<String, String>() // Matches chat id with mac address of its host
    private var myLobby:Lobby? = null // Hosted lobby reference
    private var scanned = ArrayList<String>() //To filter redundant BLE advertisement packets

    //managing contestual toolbar
    var actionMode: ActionMode? = null
    var actionModeCallback = ActionModeCallback()

    //managing RecyclerView
    lateinit var mAdapter: LobbyAdapter

    //Enables bluetooth discoverability
    private fun makeDiscoverable() {
        val discoverableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE)
        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300)
        startActivityForResult(discoverableIntent, REQUEST_ENABLE_DISCOVERABLE)
    }

    //New bluetooth device discovered
    private val discoveryFinishReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            val action = intent.action
            if (BluetoothDevice.ACTION_FOUND == action) {
                val device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE) as BluetoothDevice
                if (device.type == BluetoothDevice.DEVICE_TYPE_CLASSIC) {
                    if (device.name != null)
                        devices[device.name] = device //Store a reference
                }
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putSerializable("Lobby", myLobby) //hosted lobby
        outState.putBoolean("Discoverable", discoveryEnabled) //discoverability
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        //Recover saved instace when fragment is inflated
        if(savedInstanceState!=null){
            if(savedInstanceState.getSerializable("Lobby")!=null)
                myLobby = savedInstanceState.getSerializable("Lobby") as Lobby
            discoveryEnabled = savedInstanceState.getBoolean("Discoverable")
        }
        if(myLobby!=null) {
            lobbies.add(myLobby!!)
        }
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mAdapter = LobbyAdapter(activity!!.baseContext, lobbies, this) //initialize adapter

        //setup recyclerview
        val mLayoutManager = LinearLayoutManager(activity!!.applicationContext)
        recycler_view.layoutManager = mLayoutManager
        recycler_view.adapter = mAdapter
        recycler_view.itemAnimator = DefaultItemAnimator()
        recycler_view.addItemDecoration(DividerItemDecoration(activity!!.baseContext, LinearLayoutManager.VERTICAL))

        swipe_refresh_layout.setOnRefreshListener(this)

        //floating button setup
        fab.setOnClickListener {
            if(myLobby!=null){
                Toast.makeText(activity, "Already hosting a group!", Toast.LENGTH_SHORT).show()
            }
            else {
                val intent = Intent(activity, CreateLobbyActivity::class.java)
                startActivityForResult(intent, REQUEST_LOBBY_CREATION)
            }
        }

        //check if Bluetooth LE is supported --> YOU CAN'T CREATE LOBBIES!!!!!!
        if (!BluetoothAdapter.getDefaultAdapter().isMultipleAdvertisementSupported) {
            fab.isEnabled = false
            fab.visibility = View.INVISIBLE
            Toast.makeText(
                activity,
                "Multiple advertisement not supported",
                Toast.LENGTH_SHORT
            ).show()
        }

        //Recover saved instance
        if(savedInstanceState!=null){
            discoveryEnabled = savedInstanceState.getBoolean("Discoverable")
        }
        if(myLobby!=null) {
            if(!lobbies.contains(myLobby!!))
                lobbies.add(myLobby!!)
            mAdapter.notifyDataSetChanged()
        }

        val filters = ArrayList<ScanFilter>()

        val filter: ScanFilter = ScanFilter.Builder()
            .setServiceUuid(ParcelUuid(UUID.fromString(getString(R.string.UUID)))) //filter advertisements
            .build()
        filters.add(filter)

        val settings = ScanSettings.Builder()
            .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
            .build()

        //Setup device discovery
        val deviceFilter = IntentFilter(BluetoothDevice.ACTION_FOUND)
        activity!!.registerReceiver(discoveryFinishReceiver, deviceFilter)
        BluetoothAdapter.getDefaultAdapter().startDiscovery()

        //scanning for BLE advertisements
        val mBluetoothScanner = BluetoothAdapter.getDefaultAdapter().bluetoothLeScanner
        mBluetoothScanner.startScan(filters, settings, MyScanCallBack())

    }

    override fun onStart() {
        super.onStart()

        if(!discoveryEnabled)
            makeDiscoverable() //this device is discoverable?
    }

    override fun onDestroy() {
        super.onDestroy()
        activity!!.unregisterReceiver(discoveryFinishReceiver)
    }

    //Lobby Adapter Listener

    override fun onRowItemClicked(position: Int) {

        if(lobbies[position].id == myLobby?.id){
            //Open lobby chat as host
            val intent = Intent(activity, Chat::class.java)
            intent.putExtra("owner", true)
            intent.putExtra("title", lobbies[position].title)
            startActivity(intent)
        }
        else {
            //Open lobby chat as client
            val lobby = lobbies[position]
            val intent = Intent(activity, Chat::class.java)
            intent.putExtra("owner", false)
            intent.putExtra("device", macTable[lobby.id])
            intent.putExtra("title", lobbies[position].title)
            try {
                startActivity(intent)
            }catch(e:IOException){
                Toast.makeText(activity, "Host disconnected", Toast.LENGTH_SHORT).show()
                lobbies.remove(lobby)
                mAdapter.notifyDataSetChanged()
            }
        }
    }

    override fun onRowLongClicked(position: Int) {
        if(lobbies[position].id== myLobby?.id)
            enableActionMode(position)
        else
            Toast.makeText(activity, "You are not the owner!", Toast.LENGTH_SHORT).show()
    }

    //contextual action mode
    private fun enableActionMode(position: Int) {
        if (actionMode == null) {
            actionMode = activity?.startActionMode(actionModeCallback)
        }
        toggleSelection(position)
    }

    private fun toggleSelection(position: Int) {
        mAdapter.toggleSelection(position)
        actionMode?.title = lobbies[position].title
        actionMode?.invalidate()
    }

    private fun deleteMessages() {
        mAdapter.resetAnimationIndex()
        val selectedItemPosition = mAdapter.getSelectedItems()[0]
        lobbies.removeAt(selectedItemPosition)
        myLobby = null
        mAdapter.notifyDataSetChanged()

        val serviceIntent = Intent(activity, HostService::class.java)
        activity?.stopService(serviceIntent)
    }

    //swipe down to refresh
    override fun onRefresh() {
        makeDiscoverable()
        swipe_refresh_layout.isRefreshing = false
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_LOBBY_CREATION) {
            if (resultCode == Activity.RESULT_OK) {
                val bundle = data?.extras
                val lobby = bundle!!.getSerializable("Lobby") as Lobby
                lobby.hosted = true
                lobbies.add(lobby)
                mAdapter.notifyDataSetChanged()

                //Storing created lobby in database
                val database = FirebaseDatabase.getInstance().getReference("Lobbies").child(lobby.id)
                database.setValue(lobby)

                myLobby = lobby //saving hosted lobby reference

                val parcelUuid = ParcelUuid(UUID.fromString(resources.getString(R.string.UUID)))

                val serviceIntent = Intent(activity, HostService::class.java)
                serviceIntent.putExtra("Lobby", lobby)
                serviceIntent.putExtra("uuid", parcelUuid.uuid.toString())
                activity!!.startService(serviceIntent) //Starting hosting service!!
            }
        }
        else if(requestCode == REQUEST_ENABLE_DISCOVERABLE){
            if(resultCode == 300)
                discoveryEnabled = true
        }
    }

    //Action mode
    inner class ActionModeCallback : ActionMode.Callback {
        override fun onCreateActionMode(mode: ActionMode, menu: Menu?): Boolean {
            mode.menuInflater.inflate(R.menu.menu_action_mode, menu)
            swipe_refresh_layout.isEnabled = false
            return true
        }

        override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
            return false
        }

        override fun onActionItemClicked(mode: ActionMode, item: MenuItem): Boolean {
            return when (item.itemId) {
                R.id.action_delete -> {
                    deleteMessages()
                    mode.finish()
                    true
                }
                else -> false
            }
        }

        override fun onDestroyActionMode(mode: ActionMode) {
            mAdapter.clearSelections()
            swipe_refresh_layout.isEnabled = true
            actionMode = null
            recycler_view.post {
                mAdapter.resetAnimationIndex()
            }
        }
    }


    inner class MyScanCallBack : ScanCallback() {
        //triggered when an advertisement Bluetooth LE packet is received
        override fun onScanResult(callbackType: Int, result: ScanResult?) {
            super.onScanResult(callbackType, result)

            //checking if packet is redundant
            if(scanned.contains(result!!.scanRecord!!.serviceData.keys.elementAt(0).toString()))
                return
            else
                scanned.add(result.scanRecord!!.serviceData.keys.elementAt(0).toString())

            //getting data from the packet
            val deviceName = String(result.scanRecord!!.serviceData.values.elementAt(0))
            val id = result.scanRecord!!.serviceData.keys.elementAt(0).toString()

            if(devices[deviceName]==null){
                scanned.remove(result.scanRecord!!.serviceData.keys.elementAt(0).toString())
                return
            }

            macTable[id] = devices[deviceName]!!.address

            //using firebase as a lookup table for lobby information
            val database = FirebaseDatabase.getInstance().getReference("Lobbies").child(id)
            database.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                    TODO("Not yet implemented")
                }

                override fun onDataChange(p0: DataSnapshot) {
                    val lobby = (p0.getValue(Lobby::class.java)!!)
                    lobby.hosted = false
                    lobbies.add(lobby)
                    mAdapter.notifyDataSetChanged()
                }
            })
        }
    }
}
