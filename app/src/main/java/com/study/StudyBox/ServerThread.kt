package com.study.StudyBox

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothServerSocket
import android.bluetooth.BluetoothSocket
import android.os.Message
import android.util.Log
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.io.Serializable
import java.util.*
import kotlin.collections.ArrayList

class ServerThread(bluetoothAdapter: BluetoothAdapter, val uuid:UUID, var handler: Chat.MyHandler?): Thread(), Serializable {

    var mmServerSocket: BluetoothServerSocket? = null
    var messages = ArrayList<MyMessage>() //holds messages while chat is closed
    var chatPool = HashSet<LobbyConnectionManager>() //holds every chat instance

    init {
        var temp: BluetoothServerSocket? = null
        try {
            temp = bluetoothAdapter.listenUsingInsecureRfcommWithServiceRecord(
                "LOBBY",
                (UUID.fromString("00001101-0000-1000-8000-00805f9b34fb"))
            )
        } catch (e: IOException) {
        }
        mmServerSocket = temp
    }

    //Sends the message to any possible chat instance
    fun echo(bytes: ByteArray) {

        if (chatPool.size > 0) {

                val writtenMsg: Message = handler!!.obtainMessage(
                    Chat.MESSAGE_WRITE, -1, -1, bytes
                )
                writtenMsg.sendToTarget()

                for (i in 0 until chatPool.size) {
                    try {
                        chatPool.elementAt(i).write(bytes)
                    } catch (e: IOException) {
                        chatPool.elementAt(i).end()
                        chatPool.remove(chatPool.elementAt(i))
                        handler?.updateClientCount(chatPool.size)
                    }
                }
        } else {
            val writtenMsg: Message = handler!!.obtainMessage(
                Chat.MESSAGE_NOT_DELIVERED, -1, -1, bytes
            )
            writtenMsg.sendToTarget()
        }
    }

    override fun run() {
        super.run()
        var socket:BluetoothSocket
        while (true){
            try {
                socket = mmServerSocket?.accept()!!
            }catch (e:IOException) {
                break
            }
            val chatThread = LobbyConnectionManager(socket)
            chatThread.start()
            chatPool.add(chatThread)
            handler?.updateClientCount(chatPool.size)
        }
    }

    fun write(bytes:ByteArray){
        echo(bytes)
    }

    inner class LobbyConnectionManager(val socket: BluetoothSocket): Thread(){

        var mInputStream:InputStream? = null
        var mOutputStream:OutputStream? = null

        init {
            var tmpIn:InputStream? = null
            var tmpOut:OutputStream? = null
            try{
                tmpIn = socket.inputStream
                tmpOut = socket.outputStream
            }catch (e:IOException){
                Log.e("PROVAAA", "Can't get IO Stream", e)
            }
            mInputStream = tmpIn
            mOutputStream = tmpOut
        }

        override fun run() {
            super.run()
            val buffer = ByteArray(1024)
            var numBytes: Int

            while(true){
                try{
                    numBytes = mInputStream!!.read(buffer)
                    if(handler!=null){
                        val readMsg = handler!!.obtainMessage(Chat.MESSAGE_READ, numBytes, -1, buffer)
                        readMsg.sendToTarget()
                    }
                    else
                        addMessage(String(buffer))
                }catch(e: IOException){
                    break
                }
            }
        }

        fun write(bytes:ByteArray) {
            mOutputStream?.write(bytes)
        }

        fun end(){
            socket.close()
        }
    }

    @Synchronized
    fun  addMessage(message:String){
        messages.add(MyMessage(message, false))
    }

}